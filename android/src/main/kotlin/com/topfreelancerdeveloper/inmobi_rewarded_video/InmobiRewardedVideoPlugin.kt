package com.topfreelancerdeveloper.inmobi_rewarded_video

import android.app.Activity
import androidx.annotation.NonNull;
import com.inmobi.sdk.InMobiSdk
import com.inmobi.sdk.SdkInitializationListener

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import org.json.JSONObject

/** InmobiRewardedVideoPlugin */
public class InmobiRewardedVideoPlugin: FlutterPlugin, MethodCallHandler,ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity

  private lateinit var channel : MethodChannel
  private lateinit var activity: Activity
  private var ads = HashMap<String,RewardedAdWrapper>()



  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "inmobi_rewarded_video")
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "inmobi_rewarded_video")
      val activity = registrar.activity()
      channel.setMethodCallHandler(InmobiRewardedVideoPlugin())
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "init") {
      var testDevices : List<*>? = (call.arguments as? HashMap<*, *>)?.get("testDevices") as? List<*>?
      var appId : String? = (call.arguments as? HashMap<*, *>)?.get("appId") as? String?


      if (appId == null || appId?.length == 0){

        result.error("-1" , "empty or null appId" , null)
        return
      }
      var consentObject =  JSONObject()
      consentObject.put(InMobiSdk.IM_GDPR_CONSENT_AVAILABLE, true);
      // Provide 0 if GDPR is not applicable and 1 if applicable
      consentObject.put("gdpr", "0");
      InMobiSdk.init(activity,appId,consentObject, SdkInitializationListener {
        error -> if (error == null) result.success(true) else result.error("0" , "inmobi init failed", error.message)
      })
      if (testDevices != null)  {
        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
      }
      return
    }
    if (call.method == "create") {

      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      var forceCreate: Boolean? = (call.arguments as? HashMap<*, *>)?.get("forceCreate") as? Boolean?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if (forceCreate == null || forceCreate == false) {
        if (ads.contains(adUnitId)){
          result.error("-2" , "ad unit already has created", null)
          return
        }
      }
      ads[adUnitId] = RewardedAdWrapper(activity,adUnitId,channel)
      result.success(true)
      return
    }
    if (call.method == "load") {
      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if(ads[adUnitId] == null) {
        result.error("-2" , "ad unit id has no wrapper" , null)
        return
      }
      ads[adUnitId]?.load(result)
      return
    }
    if (call.method == "show") {
      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      var userId : String? = (call.arguments as? HashMap<*, *>)?.get("userId") as? String?
      var customData : String? = (call.arguments as? HashMap<*, *>)?.get("customData") as? String?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if(ads[adUnitId] == null) {
        result.error("-2", "ad unit id has no wrapper", null)
        return
      }
      if(ads[adUnitId]?.isLoaded() == false){
        result.error("-3", "ad not loaded", null)
        return
      }
      ads[adUnitId]?.show(userId , customData)
      result.success(true)
      return
    }
  }


  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onDetachedFromActivity() {

  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {

  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
   activity = binding.activity
  }

  override fun onDetachedFromActivityForConfigChanges() {

  }
}
