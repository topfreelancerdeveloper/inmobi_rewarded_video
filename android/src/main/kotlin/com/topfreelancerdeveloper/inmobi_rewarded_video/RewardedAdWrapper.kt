package com.topfreelancerdeveloper.inmobi_rewarded_video

import android.app.Activity
import android.util.Log
import com.inmobi.ads.AdMetaInfo
import com.inmobi.ads.InMobiAdRequestStatus
import com.inmobi.ads.InMobiInterstitial
import com.inmobi.ads.listeners.InterstitialAdEventListener
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.MethodChannel

class RewardedAdWrapper(context : Activity , adUnitId : String, channel: MethodChannel) : InterstitialAdEventListener()  {

    private var channel : MethodChannel = channel
    private var context : Activity = context
    private var adUnitId : String = adUnitId
    private var ad: InMobiInterstitial  = InMobiInterstitial (context , adUnitId.toLong() , this)
    private var loadResult : Result? = null

    fun show(userId : String?, customData : String?){

        if(isLoaded()) {
            ad.show()
        }
    }

    fun load(result : Result){
        loadResult = result
        ad.load()
    }

    override fun onAdClicked(p0: InMobiInterstitial, p1: MutableMap<Any, Any>?) {
        super.onAdClicked(p0, p1)

        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Clicked",  args)
    }

    fun isLoaded() : Boolean {
        return ad.isReady
    }

    override fun onRequestPayloadCreationFailed(p0: InMobiAdRequestStatus) {
        super.onRequestPayloadCreationFailed(p0)
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("PlaybackFailure",  args)
    }



    override fun onAdLoadSucceeded(p0: InMobiInterstitial, p1: AdMetaInfo) {
        super.onAdLoadSucceeded(p0, p1)

        if (loadResult != null) {
            loadResult?.success(true)
        }
        loadResult = null
    }

    override fun onAdLoadFailed(p0: InMobiInterstitial, p1: InMobiAdRequestStatus) {
        super.onAdLoadFailed(p0, p1)
        Log.e("inmobi_load_failed", p1.statusCode.toString())
        Log.e("inmobi_load_failed", p1.message.toString())
        if(loadResult != null) {
            loadResult?.error(p1?.statusCode?.toString(), p1?.message, null)
        }
        loadResult = null
    }



    override fun onAdDisplayed(p0: InMobiInterstitial, p1: AdMetaInfo) {
        super.onAdDisplayed(p0, p1)

        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Opened",  args)

    }



    override fun onAdDismissed(p0: InMobiInterstitial) {

        super.onAdDismissed(p0)
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Closed",  args)

    }

    override fun onRewardsUnlocked(p0: InMobiInterstitial, p1: MutableMap<Any, Any>?) {

        super.onRewardsUnlocked(p0, p1)
        var args =  HashMap<String,Any>()
        var type = ""
        var value = ""
        for ( key in p1?.keys!!) {
            if(type != "") {
                type+="|"
                value+="|"
            }
            type+="$key";
            value+="${p1[key]}"
        }
        args["adUnitId"] = adUnitId
        args["type"] = type
        args["amount"] = value
        channel?.invokeMethod("Rewarded",  args)

    }

}
