
import Foundation
import InMobiSDK

class RewardedAdWrapper : NSObject, IMInterstitialDelegate {
    
    
    var channel : FlutterMethodChannel?
    var ad : IMInterstitial?
    var loadResult : FlutterResult?
    var placementID : String?
        
    init(placementId : String , channel ch : FlutterMethodChannel) {
        super.init()
        placementID = placementId
        ad = IMInterstitial.init(placementId: Int64(placementId)!, delegate: self)
        channel = ch
    }
    
    func load(result : @escaping FlutterResult) {
        loadResult = result
        ad?.load()
    }
    
    func interstitialDidFinishLoading(_ interstitial: IMInterstitial!) {
        if loadResult != nil {
            loadResult!(true)
        }
        loadResult = nil
    }
    func interstitial(_ interstitial: IMInterstitial!, didFailToLoadWithError error: IMRequestStatus!) {
        if loadResult != nil {
            loadResult!(FlutterError(code: "-1",
                                     message: "load failed",
                                     details: error.localizedDescription
))
        }
        loadResult = nil
    }
    
    func interstitialDidPresent(_ interstitial: IMInterstitial!) {
        var args =  [String: Any]()
        args["adUnitId"] = placementID
              channel?.invokeMethod("Opened", arguments: args)

    }
    
    func interstitialDidDismiss(_ interstitial: IMInterstitial!) {
        var args =  [String: Any]()
        args["adUnitId"] = placementID
              channel?.invokeMethod("Closed", arguments: args)

    }
    
    func interstitial(_ interstitial: IMInterstitial!, didInteractWithParams params: [AnyHashable : Any]!) {
        var args =  [String: Any]()
        args["adUnitId"] = placementID
              channel?.invokeMethod("Clicked", arguments: args)
    }
    
    func interstitial(_ interstitial: IMInterstitial!, rewardActionCompletedWithRewards rewards: [AnyHashable : Any]!) {
        var args =  [String: Any]()
        var type = ""
                var value = ""
        for  key in rewards.keys {
                    if(type != "") {
                        type+="|"
                        value+="|"
                    }
                    type+="\(key)";
            value+="\(rewards[key] ?? "")"
                }

        args["adUnitId"] = placementID
        args["type"] = type
        args["amount"] = value
        channel?.invokeMethod("Rewarded", arguments: args)

    }
    
    func setServerSideDate(userId : String?, customDate : String?) {
        
//        if userId == nil {
//            return
//        }
//        if customDate != nil{
//        ad?.setRewardDataWithUserID(userId!, withCurrency: customDate!)
//            return
//        }
//        ad?.setRewardDataWithUserID(userId!, withCurrency: "")
    }

    func isReady() -> Bool? {
        return ad?.isReady()
    }
    func present(controller : UIViewController) {
        if isReady() == false {
            return
        }
        ad?.show(from: controller)
    }

}
