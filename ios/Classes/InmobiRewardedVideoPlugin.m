#import "InmobiRewardedVideoPlugin.h"
#if __has_include(<inmobi_rewarded_video/inmobi_rewarded_video-Swift.h>)
#import <inmobi_rewarded_video/inmobi_rewarded_video-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "inmobi_rewarded_video-Swift.h"
#endif

@implementation InmobiRewardedVideoPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftInmobiRewardedVideoPlugin registerWithRegistrar:registrar];
}
@end
