import Flutter
import UIKit
import InMobiSDK

public class SwiftInmobiRewardedVideoPlugin: NSObject, FlutterPlugin {
    var ads = [String : RewardedAdWrapper]()
    private static var channel = FlutterMethodChannel()

  public static func register(with registrar: FlutterPluginRegistrar) {
    channel = FlutterMethodChannel(name: "inmobi_rewarded_video", binaryMessenger: registrar.messenger())
    let instance = SwiftInmobiRewardedVideoPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
       if call.method == "init" {
               let testDevices: [String]? = (call.arguments as? [String: Any])?["testDevices"] as? [String]
        let appId: String? = (call.arguments as? [String: Any])?["appId"] as? String
        if appId == nil || appId?.count == 0{
            result(FlutterError(code: "-1",
                message: "empty or null appId",
                details: nil)
            )
            return
        }
        var args =  [String: Any]()
        args[IM_GDPR_CONSENT_AVAILABLE] = true
        args["gdpr"] = 1//inin
        IMSdk.initWithAccountID(appId!, consentDictionary: args)
               if testDevices != nil {
                   //testdevcie
                IMSdk.setLogLevel(.debug)
               }
               result(true)
               return
           }
       if call.method == "create" {
               let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
               let forceCreate: Bool? = (call.arguments as? [String: Any])?["forceCreate"] as?Bool
               if adUnitId == nil || adUnitId?.count == 0{
                   result(FlutterError(code: "-1",
                       message: "empty or null adunitid",
                       details: nil)
                   )
                   return
               }
               if forceCreate == nil || forceCreate == false {
               if ads.contains(where: { (key: String, value: RewardedAdWrapper) -> Bool in
                   key == adUnitId
               }){
                  result(FlutterError(code: "-2",
                       message: "ad unit already has created",
                       details: nil)
                   )
                   return
               }
               }
           ads[adUnitId ?? ""] = RewardedAdWrapper(placementId: adUnitId!, channel: SwiftInmobiRewardedVideoPlugin.channel)
               result(true)
               return
           }
       if call.method == "load" {
               let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
               if adUnitId == nil || adUnitId?.count == 0{
                   result(FlutterError(code: "-1",
                       message: "empty or null adunitid",
                       details: nil)
                   )
                   return
               }
               
               if ads[adUnitId ?? ""] == nil {
                   result(FlutterError(code: "-2",
                       message: "ad unit id has no wrapper",
                       details: nil)
                   )
                   return
               }
               
               ads[adUnitId ?? ""]?.load(result: result)
               return
           }
       if call.method == "show" {
               let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
           let userId: String? = (call.arguments as? [String: Any])?["userId"] as? String
           let customData: String? = (call.arguments as? [String: Any])?["customData"] as? String
               if adUnitId == nil || adUnitId?.count == 0{
                   result(FlutterError(code: "-1",
                       message: "empty or null adunitid",
                       details: nil)
                   )
                   return
               }
               
               if ads[adUnitId ?? ""] == nil {
                   result(FlutterError(code: "-2",
                       message: "ad unit id has no wrapper",
                       details: nil)
                   )
                   return
               }
           
           ads[adUnitId ?? ""]?.setServerSideDate(userId: userId, customDate: customData)
               
               if (ads[adUnitId ?? ""]?.isReady() ?? false) == false{
                   result(FlutterError(code: "-3",
                       message: "not loaded",
                       details: nil)
                   )
                   return
               }
               ads[adUnitId ?? ""]?.present(controller: (UIApplication.shared.delegate?.window??.rootViewController)!)
               result(true)
               return
           }
       result("iOS " + UIDevice.current.systemVersion)



     }

}
